package logica;

public class Esfera extends Circulo implements Cuerpo {
	public Esfera(double valor1) {
		super(valor1);
	}
	
	@Override
	public double volumen() {
		return Math.PI*Math.pow(this.valor1, 3)*4/3;
	}
}
