package logica;


public class PIRAMIDE extends Cuadrado implements Cuerpo  {
    private double valor2;
    
    public PIRAMIDE (double valor1,double valor2) {
    super (valor1);
    this.valor2=valor2;
   
    }
    
	public double getValor2() {
		return valor2;
	}

	public void setValor2(double valor2) {
		this.valor2 = valor2;
	}

	@Override
	public double volumen() {
		return (Math.pow(this.valor1, 2)*valor2)/3;
	
    
    }
}
